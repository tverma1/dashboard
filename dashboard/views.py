from django.http import HttpResponse
from django.shortcuts import redirect, render
from django.contrib.auth import authenticate, logout, login
from django.contrib.auth.models import User

def index(request):
    return HttpResponse('welcome home')

def dashboard(request):
    if not request.user.username == 'tanu':
        return HttpResponse('unauthorized')
    return HttpResponse('<h1>Dashboard</h1>')

def login_user(request):
    if request.method == 'POST':
        username = request.POST['username']
        password = request.POST['password']
        if(username == ''): return HttpResponse('No User')
        if(password == ''): return HttpResponse('No password')
        user = authenticate(username = username, password = password)
        if (user is None): return HttpResponse('Unauthorized!')
        else:
            login(request, user)
            return redirect('/')
    return render(request, 'login.html')

'''
def register(request):
    if request.method == 'POST':
        username = request.POST['username']
        password = request.POST['password']
        email = request.POST['email']
        if(username == ''): return HttpResponse('No User')
        if(password == ''): return HttpResponse('No password')
        if(email == ''): return HttpResponse('No email')
        user.objects.created_user(username = username, password = password, email = email)
        if (user is None): 
            return HttpResponse('Unauthorized!') 
        else: 
            login(request, user)
            return redirect('/')
    return render(request, 'login.html')'''


def username(request):
    return HttpResponse('your username is:' +request.user.username)

def logout_user(request):
    #perfoming automatic logout 
    logout(request)
    return HttpResponse('logout')